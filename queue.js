let collection = [];

// Write the queue functions below.
// Note: Avoid using Array methods (except .length) on creating the queue functions.

function print() {
  return collection;
}

function enqueue(John) {
  collection.push(John);
  return collection;
}

module.exports = {
  //export created queue functions
  print,
};
